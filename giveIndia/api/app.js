var express = require("express");
var app = express();
const morgan = require('morgan');
var bodyParser = require("body-parser");

/**
 * parse requests of content-type - application/json
 */
app.use(bodyParser.json());
/**
 * parse requests of content-type - application/x-www-form-urlencoded
 */
app.use(bodyParser.urlencoded({ extended: false }));
app.get('/', (req, res) => {
    res.json({"message": "Congratulations! you are working great!"});
});
app.use(morgan('dev'))
app.use("/app", require("./routes/user_route"));
app.use("/app", require("./routes/bankTransfer_route"));

app.use(function(req, res, next) {  
    req.getUrl = function() {
      return req.protocol + "://" + req.get('host') + req.originalUrl;
    }
    return next();
  });


app.listen(9000);

console.log("Listening to PORT 9000");

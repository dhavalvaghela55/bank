const express = require("express");
const router = express.Router();

const bankControllr = require("../controller/bankTransfer_controller");


router.post("/bank", bankControllr.create);

module.exports = router;

const mongoose = require("../db");
const schema = new mongoose.Schema(
  {
    name: {
      desc: "The user's name.",
      trim: true,
      type: String,
      required: true,
    },
    age: {
      desc: "The users's age.",
      type: Number,
    },
    balance : {
      desc: "The users's balance.",
      type: Number,
    },
    city: {
      desc: "The user's city.",
      trim: true,
      type: String,
      required: true,
    },
    accountType: {
      desc: "The user's accountType.",
      trim: true,
      type: String,
      required: true,
    },
  },
  {
    strict: true,
    versionKey: false,
    timestamps: { createdAt: "createdAt", updatedAt: "updatedAt" },
  }
);

module.exports = mongoose.model("User", schema);
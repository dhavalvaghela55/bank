const mongoose = require("../db");
const schema = new mongoose.Schema(
  {
    fromUser: {
      desc: "The user's name.",
      type: String,
      required: true,
    },
    toUser: {
      desc: "The user's name.",
      type: String,
      required: true,
    },
    amount : {
      desc: "The users's balance.",
      type: Number,
    },

  },
  {
    strict: true,
    versionKey: false,
    timestamps: { createdAt: "createdAt", updatedAt: "updatedAt" },
  }
);

module.exports = mongoose.model("BankTransaction", schema);
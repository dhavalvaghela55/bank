/**
 * User controller : All business logic goes here
 */
const User = require("../model/user_model");
const bcrypt = require("bcryptjs");
/**
 * this method is to create the user
 */
exports.create = (req, res) => {
  /**
   * validation request
   */
  console.log('req.body',req.body)
  if (!req.body.age || !req.body.city || !req.body.name || !req.body.accountType) {
    return res.status(400).send({
      message: "Required field can not be empty",
    });
  }
  /**
   * Create a user
   */
  const user = new User({
    name: req.body.name,
    age: req.body.age,
    city: req.body.city,
    accountType: req.body.accountType,
    balance:0
  });
  /**
   * Save user to database
   */
  user
    .save()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the User.",
      });
    });
};


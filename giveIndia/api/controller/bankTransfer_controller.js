/**
 * User controller : All business logic goes here
 */

const Users = require("../model/user_model");
const Bank = require("../model/bank_model");
const async = require("async");
const ObjectID = require('mongodb').ObjectID;
const moment = require('moment')
/**
 * this method is to create the order
 */
exports.create = (req, res) => {
  /**
   * validation request
   */
  if (!req.body.fromAccountId || !req.body.toAccountId || !req.body.amount) {
    return res.status(400).send({
      message: "Required field can not be empty",
    });
  }
  var newSrcBalance;
  var totalDestBalance;

  let fetchUsers = (data) => {
    return new Promise((resolve, reject) => {
      Users.find({ _id: req.body.fromAccountId }, function (err, resData) {
        if (err) {
          reject(err);
        }
        else {
          if (resData.length > 0) {
            if (req.body.toAccountId == req.body.fromAccountId) {
              return res.status(404).send({
                message: "you can not transfer amount to your same account",
              });
            }
            var getFromBalance = resData[0]['balance'];
            if (req.body.amount > getFromBalance) {
              return res.status(404).send({
                message: "insufficient account balance",
              });
            }
            Users.find({ _id: req.body.toAccountId }, function (err, resDataTo) {
              if (err) {
                reject(err);
              }
              else {
                if (resDataTo.length > 0) {
                  let balanceTo = resDataTo[0]['balance'];
                  let checkAddBalance = balanceTo + req.body.amount;
                  if (resDataTo[0]['accountType'] == "BasicSavings" && checkAddBalance > 50000) {
                    return res.status(404).send({
                      message: "Exceed limit of Rs. 50,000 on BasicSavings",
                    });
                  } else {
                    newSrcBalance = resData[0]['balance'] - req.body.amount;
                    const filterFromId = { _id: req.body.fromAccountId };
                    const updateFromId = { 'balance': newSrcBalance }
                    Users.findOneAndUpdate(filterFromId, updateFromId, {
                    }, (err, result) => {
                      if (err) {
                        return res.status(400).send({
                          message: "server error",
                        });
                      }
                      else {
                        totalDestBalance = resDataTo[0]['balance'] + req.body.amount;
                        const filterToId = { _id: req.body.toAccountId };
                        const updateToId = { 'balance': totalDestBalance }
                        Users.findOneAndUpdate(filterToId, updateToId, {
                        }, (err, result) => {
                          if (err) {
                            return res.status(400).send({
                              message: "server error",
                            });
                          } else {
                            resolve(true);
                          }
                        })
                      }
                    });
                  }
                } else {
                  return res.status(400).send({
                    message: "user not found",
                  });
                }
              }
            });
          } else {
            return res.status(400).send({
              message: "user not found",
            });
          }
        }
      });
    })
  }

  let createTransaction = (data) => {
    return new Promise((resolve, reject) => {

      const banks = new Bank({
        fromUser: req.body.fromAccountId,
        toUser: req.body.toAccountId,
        amount: req.body.amount,
      });

      banks
        .save()
        .then((data) => {
          resolve(true);
        })
        .catch((err) => {
          res.status(500).send({
            message: err.message || "Some error occurred while creating the User.",
          });
        });

    })
  }

  fetchUsers()
    .then(createTransaction)
    .then((data) => {
      res.status(200).send({ 'data ': { 'newSrcBalance': newSrcBalance, 'totalDestBalance' : totalDestBalance,'transferedAt': moment().unix() } });
    }).catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while getting the record.",
      });
    });


};
